package portecho_nio;

import port_channel.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.*; 
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;

public class ChannelPort implements Runnable{
    //The host:port combination to monitor
    private InetAddress hostAddress;
    private int port;
    //The channel on which to accept connections
    private ServerSocketChannel serverChannel;
    //the selector to monitor;
    private Selector selector;
    //the buffer into which to read data as it's available
    private ByteBuffer readBuffer;
    //A queue where the messages are stored
    ConcurrentLinkedQueue<ByteBuffer> que;
    //The size of the network
    private int networkSize;
    //Charset decoder
    CharsetDecoder decoder;

    public ChannelPort(String hostAddress, int port){
        try{
            this.hostAddress = InetAddress.getByName(hostAddress);
        } catch(UnknownHostException e){
            System.err.println("Unknown Host");
            System.exit(-1);
        }
        this.port = port;
        this.readBuffer = ByteBuffer.allocate(8192);
        this.que = new ConcurrentLinkedQueue<ByteBuffer>();
        this.networkSize = 0;
        this.selector = this.initSelector();
        this.decoder = Charset.forName("UTF-8").newDecoder();
    }

	protected Selector initSelector(){
        Selector socketSelector = null;
        try{
            //create a new selector
            socketSelector = SelectorProvider.provider().openSelector();
            
            //create a new non-blocking server socket channel
            this.serverChannel = ServerSocketChannel.open();
            this.serverChannel.configureBlocking(false);

            //Bind the server socket to the specified address and port
            InetSocketAddress isa = new InetSocketAddress(this.hostAddress, this.port);
            this.serverChannel.socket().bind(isa);

            //Register the new server socket channel, indicating interest in accepting new connections
            this.serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
        }catch(IOException e){
            e.printStackTrace();
        }
        return socketSelector;
    }
    
    /**
     * Busy waits until a channel registered with the selector is in an acceptable or readable state.
     */
    public void run(){
        Iterator selectedKeys = null;
        SelectionKey key = null;
        System.out.println("Awaiting connections...");

        while(true){
            try{
                //wait for an event on one of the registered channels
                this.selector.select();

                //Iterate over the set of keys for which events are available
                selectedKeys = this.selector.selectedKeys().iterator();

                while(selectedKeys.hasNext()){
                    key = (SelectionKey) selectedKeys.next();
                    selectedKeys.remove();

                    if(!key.isValid()){
                        continue;
                    }

                    //Check the available event and handle it
                    if(key.isAcceptable()){
                        this.accept(key);
                    } else if(key.isReadable()){
                        this.read(key);
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    

    //TO DO: once this works, go back and pass the ServerSocketChannel, SocketChannel and Socket as arguments
    private void accept(SelectionKey key) throws IOException{
        //Set up a server socket channel
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

        //Accept the connection and set is as non-blocking
        SocketChannel socketChannel = serverSocketChannel.accept();
        Socket socket = socketChannel.socket();
        socketChannel.configureBlocking(false);

        //register the SocketChannel with the Selector. This says you want to be notified when data is waiting to be read
        socketChannel.register(this.selector, SelectionKey.OP_READ);
        this.networkSize += 1;
    }

    private void read(SelectionKey key) throws IOException{
        SocketChannel socketChannel = (SocketChannel) key.channel();

        //Clear the read buffer for new data
        this.readBuffer.clear();

        //Attempt to read the channel
        int numRead;
        try{
            numRead = socketChannel.read(readBuffer);

            //flip the buffer to be read from
            this.readBuffer.flip();

            //TODO: This is going to have to involve some parsing logic with another method call.
            gotMessage(readBuffer);
            //Receive the message then broadcast the it
            //This has the effect of always broadcasting the first mesage atop the queue
            System.out.println("Received: " + this.que.peek().toString());

            /*
            CharBuffer c = decoder.decode(this.que.peek());
            System.out.println("Received: " + c.toString());
            */

            broadcast(this.selector, receive());
        } catch(IOException e){
            //The remote closed the session. Cancel the key and close the channel.
            key.cancel();
            socketChannel.close();
            this.networkSize -=1;
            return;
        }

        if(numRead == -1){
            //In this case, the remote shut the socket down entirely. Close it on this end as well.
            key.channel().close();
            key.cancel();
            this.networkSize -=1;
            return;
        }
    }

	synchronized void gotMessage(ByteBuffer message) {
		this.que.offer(message); 
		notifyAll(); 
	} 
	
	public synchronized ByteBuffer receive() { 
		while (que.isEmpty()) { 
			try {
				wait(); 
			} catch (InterruptedException ire) {
				ire.printStackTrace();
			}
		} 
		ByteBuffer msg = que.poll(); 

		return msg; 
	} 

    /**
     * Broadcasts the first message atop the que.
     */
    private void broadcast(Selector selector, ByteBuffer messageBuffer) throws IOException{

        for(SelectionKey key : selector.keys()){
            if(key.isValid() && key.channel() instanceof SocketChannel){
                SocketChannel sc = (SocketChannel) key.channel();
                sc.write(messageBuffer);
                messageBuffer.rewind();
            }
        }
    }

    public static void main(String[] args){
		if (args.length != 2){
			System.out.println("usage: java ChannelPort port-number number-of-nodes");
			System.exit(-1);
		} 
		int portNum = Integer.parseInt(args[0]); 
        int numNode = Integer.parseInt(args[1]); 

        new Thread(new nio_portchannel.ChannelPort("127.0.0.1", portNum)).start();

    }
}

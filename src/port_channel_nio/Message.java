package port_channel_nio;

import java.io.Serializable;
import java.sql.Timestamp;

public abstract class Message implements Serializable {
	protected int pId;
	protected char msgType;
	protected String timestamp; 

	public Message(int pId, char msgType) {
		this.pId = pId;
		this.msgType = msgType;
		this.timestamp = new Timestamp(System.currentTimeMillis()).toString();
	}

	public int getPID(){
		return this.pId;
	}

	public char messageType(){
		return this.msgType;
	}

	public abstract String toString();



	public static Message parse(String message){
		String[] s = message.split("\\|");
		Message m = new MessageString(s[3], Integer.parseInt(s[1]), s[0].charAt(0), s[2]);
		return m;
	}
}
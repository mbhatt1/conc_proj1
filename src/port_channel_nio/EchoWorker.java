package port_channel_nio;



import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.*; 
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;
import java.util.*;


class EchoWorker implements Runnable {
    private final List<ServerDataEvent> queue = new LinkedList<ServerDataEvent>();
    private Selector s; 
    private HashSet<SocketChannel> scl;


    public void setSelector(HashSet<SocketChannel> scl){
        this.scl = scl;
    }

    public void processData(ChannelPort server, Selector se, SocketChannel socket, byte[] data, int count, HashSet<SocketChannel> scl) {
        byte[] dataCopy = new byte[count];
        // this.scl = scl;
        this.scl = scl;
        this.s = server.selector;
            
        System.arraycopy(data, 0, dataCopy, 0, count);
        synchronized (queue) {
            queue.add(new ServerDataEvent(server, socket, dataCopy));
            queue.notify();
        }
    }

    public void run() {
        while (true) {
            ServerDataEvent dataEvent;

            // Wait for data to become available
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                    }
                }
                dataEvent = queue.remove(0);
            }

                // dataEvent.server.send(dataEvent.socket, dataEvent.data);
                System.out.println(queue.size());
                
        
                System.out.println("Total Keys" + this.s.keys().size());
                for (SelectionKey key : this.s.keys()){
                    
                    if (key.isValid() & (key.channel() instanceof SocketChannel)){
                                try{
                                    SocketChannel sc = (SocketChannel) key.channel();
                                     sc.write(ByteBuffer.wrap(dataEvent.data));
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                    }                            
                }
            
        
        }



    }
}


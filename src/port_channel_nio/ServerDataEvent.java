
package port_channel_nio;



import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.*; 
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;
import java.util.*;

public class ServerDataEvent {
        public ChannelPort server;
        public SocketChannel socket;
        public byte[] data;
    
        public ServerDataEvent(ChannelPort server, SocketChannel socket, byte[] data) {
            this.server = server;
            this.socket = socket;
            this.data = data;
        }
    }

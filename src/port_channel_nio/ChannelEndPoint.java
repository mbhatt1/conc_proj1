package port_channel_nio;



import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.net.Socket;
import java.util.Iterator;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.*; 
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.*;



public class ChannelEndPoint implements Runnable{

    private InetAddress hostAddress;
    private int port;

    // The selector we'll be monitoring
    private Selector selector;

    // The buffer into which we'll read data when it's available
    private ByteBuffer readBuffer = ByteBuffer.allocate(8192);

    // A list of PendingChange instances
    private final List<ChangeRequest> pendingChanges = new LinkedList<ChangeRequest>();

    private int nodeId;

    // Maps a SocketChannel to a list of ByteBuffer instances
    private final Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<SocketChannel, List<ByteBuffer>>();

    public ChannelEndPoint(InetAddress hostAddress, int port, int Id) throws IOException {
        this.hostAddress = hostAddress;
        this.port = port;
        this.nodeId = Id;
        initSelector();
    }

    public void send(SocketChannel socket, byte[] data) throws IOException {
        // And queue the data we want written

        synchronized (pendingData) {
            List<ByteBuffer> queue = pendingData.get(socket);
            if (queue == null) {
                queue = new ArrayList<ByteBuffer>();
                pendingData.put(socket, queue);
            }
            queue.add(ByteBuffer.wrap(data));
        }

        // Finally, wake up our selecting thread so it can make the required changes
        selector.wakeup();
    }

    public void run() {
        test(2);
        while (true) {
            try {
                // Process any pending changes
                synchronized (pendingChanges) {
                    for (ChangeRequest pendingChange : pendingChanges) {   
                        switch (pendingChange.type) {
                            case ChangeRequest.CHANGEOPS:
                                SelectionKey key = pendingChange.socket.keyFor(selector);
                                key.interestOps(pendingChange.ops);
                                break;
                            case ChangeRequest.REGISTER:
                                pendingChange.socket.register(selector, pendingChange.ops);
                                break;
                        }
                    }
                    pendingChanges.clear();
                }

                // Wait for an event one of the registered channels
                selector.select();

                // Iterate over the set of keys for which events are available
                Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = selectedKeys.next();
                    selectedKeys.remove();

                    if (!key.isValid()) {
                        continue;
                    }

                    // Check what event is available and deal with it
                    if (key.isConnectable()) {
                        finishConnection(key);
                    } else if (key.isReadable()) {
                        // System.out.println("Here in Read");
                        read(key);
                    } else if (key.isWritable()) {

                        // System.out.println("Here in Write");
                        write(key);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        // Clear out our read buffer so it's ready for new data
        readBuffer.clear();

        // Attempt to read off the channel
        int numRead;
        try {
            numRead = socketChannel.read(readBuffer);
        } catch (IOException e) {
            // The remote forcibly closed the connection, cancel
            // the selection key and close the channel.
            key.cancel();
            socketChannel.close();
            return;
        }

        if (numRead == -1) {
            // Remote entity shut the socket down cleanly. Do the
            // same from our end and cancel the channel.
            key.channel().close();
            key.cancel();
            return;
        }
        // key.interestOps(SelectionKey.OP_WRITE);
        // Handle the response
        handleResponse(socketChannel, readBuffer.array(), numRead);

    }

    private void handleResponse(SocketChannel socketChannel, byte[] data, int numRead) throws IOException {
        // Make a correctly sized copy of the data before handing it
        // to the client
        byte[] rspData = new byte[numRead];
        System.arraycopy(data, 0, rspData, 0, numRead);
        // socketChannel.close();
        // socketChannel.keyFor(this.selector).cancel();

        System.out.println("Received " + new String (rspData, "UTF-8"));
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        synchronized (pendingData) {
            List<ByteBuffer> queue = pendingData.get(socketChannel);

            // Write until there's not more data ...
            while (queue != null && !queue.isEmpty()) {
                ByteBuffer buf = queue.get(0);
                socketChannel.write(buf);
                if (buf.remaining() > 0) {
                    // ... or the socket's buffer fills up
                    break;
                }
                queue.remove(0);
            }

            if (queue == null || queue.isEmpty()) {
                // We wrote away all data, so we're no longer interested in
                // writing on this socket. Switch back to waiting for data.
                // System.out.println("Do I ever get here?"); 
                key.interestOps(SelectionKey.OP_READ);
                
            }
        }
        selector.wakeup();
    }

    private void finishConnection(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        try {
            socketChannel.finishConnect();
        } catch (IOException e) {
            // Cancel the channel's registration with our selector
            key.cancel();
            return;
        }

        key.interestOps(SelectionKey.OP_WRITE);
    }

    private SocketChannel initiateConnection() throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);

        // Kick off connection establishment
        socketChannel.connect(new InetSocketAddress(hostAddress, port));
        
        synchronized (pendingChanges) {
            pendingChanges.add(new ChangeRequest(socketChannel, ChangeRequest.REGISTER, SelectionKey.OP_CONNECT));
        }

        return socketChannel;
    }

    private void initSelector() throws IOException {
        // Create a new selector
        this.selector = SelectorProvider.provider().openSelector();
    }

    public void test(int ThreadCount){
        SocketChannel sc = null;
        try{

            sc = initiateConnection();
            Thread.sleep(60000);
        } catch(Exception e){
            e.printStackTrace();
        }
        int j = 0;
        for(int i = 0; i < 10; i ++ ){

            String message = "A" + "-Message-" + this.nodeId + "-" + i + ",";
            MessageString msg = new MessageString(message, this.nodeId, 'T');
            try{
                this.send( sc,  msg.toString().getBytes());
            } catch (Exception e){
                e.printStackTrace();
            }     
            j = i;       
        }
        System.out.println("Messages Sent " + j);

    }


    public static void main(String[] args) throws InterruptedException{
		if (args.length != 3){
			System.out.println("usage: java ChannelEndPoint server-ip server-port nodes-ID");
			System.exit(-1);
		} 
		int portNum = Integer.parseInt(args[1]); 
        int nodeId = Integer.parseInt(args[2]); 
        String address = args[0];
        try{
            ChannelEndPoint nc = new ChannelEndPoint(InetAddress.getByName(address), portNum, nodeId);
            Thread t = new Thread(nc);
            t.start();
            // nc.test(2);
            
        }catch (IOException e){
            e.printStackTrace();
        }
        


        
    }



}
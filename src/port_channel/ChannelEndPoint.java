package port_channel;
import java.io.*;
import java.net.*;
import java.util.concurrent.*; 
import java.util.Random;

public class ChannelEndPoint { 
	int nodeId;
	ObjectOutputStream out; 
	BufferedReader br; 
	Socket soc = null; 
	String ip; 
	int port; 
	ConcurrentLinkedQueue<Message> que;
	
	public ChannelEndPoint(int nodeId, String ip, int port) {
		this.nodeId = nodeId; 
		this.ip = ip;
		this.port = port; 
		this.que = new ConcurrentLinkedQueue<Message>(); 
	}

	public void initialize() throws InterruptedException { 
		while (soc == null) {
			try {
				soc = new Socket(ip, port); 
				System.out.println("Socket open. Connection established."); 
				br = new BufferedReader(new InputStreamReader(soc.getInputStream())); 
				out = new ObjectOutputStream(soc.getOutputStream());
			} catch (UnknownHostException e) {
				System.err.println("ERROR: Unknown host."); 
			} catch (IOException e) {
				System.err.println("ERROR: Couldn't get I/O for the connection to server.");
			} 
			//Thread.sleep(1000); 
		}
	} 

	public synchronized void gotMessage(Message message) {
		que.offer(message); 
		notifyAll(); 
	} 
	
	public synchronized Message receive() { 
		while (que.isEmpty()) { 
			try {
				wait(); 
			} catch (InterruptedException ire) {
				ire.printStackTrace();
			}
		} 
		Message msg = que.poll(); 

		return msg; 
	} 
	
	public void send(Message msg) { 
		try { 
			out.writeObject(msg);
		} catch (SocketException se) { 
			System.exit(1);
		} catch (IOException ioe) {
			ioe.printStackTrace(); 
		}
	}

	public synchronized ObjectInputStream getObjectInputStream(){
		ObjectInputStream in = null;
		try{
			in = new ObjectInputStream(soc.getInputStream());
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return in;
	}
	
	public void close() throws IOException { 
		try { 
			send(new MessageString("Shutting down.", 1, 'E'));
			//System.out.println("Shutting down."); 
		} catch (Exception e) { 
			e.printStackTrace();
		} finally {
			br.close();
			out.close();
			soc.close();
		}
	}
	
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException { 
		if (args.length != 3){ 
			System.out.println("usage: java ChannelEndPoint server-ip server-port nodes-ID");
			System.exit(-1);
		} 
		int nodeId = Integer.parseInt(args[2]);
		String ip = args[0]; 
		int port = Integer.parseInt(args[1]); 
		ChannelEndPoint cep = new ChannelEndPoint(nodeId, ip, port); 
		cep.initialize(); 
        Thread.sleep(10000);
        Random rand = new Random();
        
		// int num_messages = 0;
		ReadThread rt = new ReadThread(cep);
		Thread td = new Thread(rt);	
		td.start();

		for (int j = 0; j < 5; j++) {
			Message msg = new MessageTest(nodeId, Integer.toString(Math.abs(rand.nextInt())), 'T'); 
			System.out.println("sending: " + msg);
			try {
				cep.send(msg);
                Thread.sleep(2000);

			} catch (Exception e) {
				e.printStackTrace();
            }


        } 

		cep.close();
	}


}



package port_channel;

import java.io.Serializable;

public class MessageString extends Message {

	String message;
	String timestamp = null;

	public MessageString(String message, int pId, char msgType) {
		super(pId, msgType);
		this.message = message;
		this.timestamp = super.timestamp;
	}
	
	public MessageString(String message, int pId, char msgType, String timestamp) {
		super(pId, msgType);
		this.message = message;
		this.timestamp = timestamp;
	}

	public String getMessage(){
		return this.message;
	}

	public String toString() {
		return (super.msgType + "|" + super.pId + "|" + this.timestamp + "|Message: " + this.message);
	}
}

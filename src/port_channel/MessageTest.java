package port_channel;

import java.io.Serializable;

import java.io.Serializable;

public class MessageTest extends Message {
	private String msg;
	public MessageTest(int pId, String msg, char msgType) {
		super(pId, msgType);
		this.msg = msg;
	}

	public String toString() {
		return (super.msgType + "|" + super.pId + "|" + super.timestamp + "| " + this.msg + "| " + this.receiveTimeStamp);
	}


}
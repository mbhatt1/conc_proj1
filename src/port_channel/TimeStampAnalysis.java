package port_channel;

import java.util.HashMap;

import java.sql.Timestamp;

public class TimeStampAnalysis {
    private HashMap<Timestamp, Timestamp> timeStamps = new HashMap<Timestamp, Timestamp>();

    public TimeStampAnalysis(){

    }

    public void addTimeStamps(Timestamp startTime, Timestamp endTime){
        timeStamps.put(startTime, endTime);
    }

    public float computeAverage(){
        long totalTime = 0;
        long totalMessages = timeStamps.size();

        for (Timestamp s : timeStamps.keySet()){
            long startTime = s.getTime();
            long stopTime = timeStamps.get(s).getTime();
            totalTime += startTime - stopTime;
        }

        return totalTime/(float)totalMessages;
    }

}
package port_channel;
import java.io.*;
import java.net.*;
import java.sql.Timestamp;
import java.util.concurrent.*; 
import java.util.Random;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ReadThread implements Runnable{
	private ChannelEndPoint cep;
	private	ObjectInputStream in;
	private TimeStampAnalysis ts;
	private boolean fromTester = false;

	public ReadThread(ChannelEndPoint cep){
		this.cep = cep;
		this.in = cep.getObjectInputStream();
	}

	public ReadThread(ChannelEndPoint cep, boolean fromTester){
		this.cep = cep;
		this.in = cep.getObjectInputStream();
		this.fromTester = fromTester;
		this.ts = new TimeStampAnalysis();
	}

	public Timestamp parseStringToTime(String yourString){
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
			Date parsedDate = dateFormat.parse(yourString);
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			return timestamp;
		} catch(Exception e) { 
			e.printStackTrace();
		}
		return null;
	}
	

	public void run(){
		try{
		MessageTest msg1 = (MessageTest) in.readObject();
		// this.ts = new TimeStampAnalysis();

		while (msg1 != null){
			try{

				if (msg1.pId == cep.nodeId){
					msg1.setTimeStamp();
					
					cep.gotMessage(msg1);
					if (fromTester == true){

						ts.addTimeStamps(parseStringToTime(msg1.timestamp), parseStringToTime(msg1.receiveTimeStamp));
						System.out.println("Average Time: " + Float.toString(ts.computeAverage()));
					}


				
				}

				System.out.println("Client " + msg1.pId + ": " + msg1 + "");
				
				MessageTest announce = (MessageTest) cep.receive();
				// System.out.println(announce); 

				if(announce == null){
					System.out.println("EXIT.");
					
					
					break;
				}
				msg1 = (MessageTest) in.readObject();
				if (msg1 == null){
					break;
				}
			} catch(ClassNotFoundException e){
				e.printStackTrace();
			} catch (EOFException e){
				cep.close();
				System.exit(0);
			}catch (SocketException e){
				cep.close();
				System.exit(0);
			}

		} 
		// if (this.fromTester == true)
		// 	System.out.println("Average Time: " + Float.toString(this.ts.computeAverage()));

	}catch(Exception e){
		e.printStackTrace();
	}
}




}
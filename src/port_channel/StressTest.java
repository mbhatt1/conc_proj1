package port_channel;

import java.nio.channels.Channel;
import java.io.*;
import java.net.*;
import java.util.concurrent.*; 
import java.util.Random;

public class StressTest implements Runnable{
    private ChannelEndPoint cep;
    private Random rand;
    public StressTest(ChannelEndPoint cep) throws InterruptedException{
        this.cep = cep;
        this.cep.initialize();
        this.rand = new Random();
    
    }

    public void sendMessage(){
        (new Thread(new SendThread(this.cep))).start();


    }

    public void receiveMessage(){
        Thread receiveThread = new Thread(new ReadThread(this.cep, true));
        receiveThread.start();
    }

    public void run(){
        sendMessage();
        receiveMessage();

    }


    public static void main(String[] args) throws InterruptedException{
        if (args.length != 3){ 
			System.out.println("usage: java StressTest server-ip server-port nodes-ID");
			System.exit(-1);
		} 
        
        int nodeId = Integer.parseInt(args[2]);
		String ip = args[0]; 
		int port = Integer.parseInt(args[1]); 
        ChannelEndPoint cep = new ChannelEndPoint(nodeId, ip, port);
        StressTest st = new StressTest(cep);
        Thread.sleep(60000); 
        (new Thread(st)).start();
    }


    


}

import port_channel_nio.*;


public class MatrixWorkerThread{
    private Matrix A;
    private Matrix B;
    private ChannelEndPoint up;
    private ChannelEndPoint left;
    private ChannelPort for_down;
    private ChannelPort for_right;
    private EchoWorker for_down_echo;
    private EchoWorker for_right_echo;
    private MatrixWorkerThread up_ref;
    private MatrixWorkerThread left_ref;
    private Thread downThread;
    private Thread downWorkerThread;
    private Thread rightThread;
    private Thread rightWorkerThread;
    private boolean ready = false;

    //Use 2222 for receiving from down
    //Use 2223 for receiving from up


    public MatrixWorkerThread(Matrix A, Matrix B){
        this.A = A;
        this.B = B;
        this.for_down_echo = new EchoWorker();
        this.for_right_echo = new EchoWorker();
        this.forDown = new ChannelPort(InetAddress.getByName("127.0.0.1"), 2222, worker);
        this.forRight = new ChannelPort(InetAddress.getByName("127.0.0.1"), 2223, worker);
    }

    public void setTopAndLeft(MatrixWorkerThread left, MatrixWorkerThread top){
        this.up_ref = top;
        this.left_ref = left;
    }

    public void startChannelPorts(){
        //Receiving from bottom and right
        this.downWorkerThread = new Thread(this.for_down_echo);
        this.downWorkerThread.start();
        this.rightWorkerThread = new Thread(this.for_right_echo);
        this.rightWorkerThread.start();
        this.downThread = new Thread(this.for_down);
        this.downThread.start();
        this.rightThread = new Thread(this.for_right);
        this.rightThread.start();

    }

    public void startChannelEndpoints(){
        this.up = new ChannelEndPoint(hostAddress, port)
    }





    
}